<!-- ================== BEGIN BASE JS ================== -->
<script src="assets/backend/plugins/jquery/jquery-1.9.1.min.js"></script>
<script src="assets/backend/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
<script src="assets/backend/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
<script src="assets/backend/plugins/bootstrap/js/bootstrap.min.js"></script>
<!--[if lt IE 9]>
	<script src="assets/backend/crossbrowserjs/html5shiv.js"></script>
	<script src="assets/backend/crossbrowserjs/respond.min.js"></script>
	<script src="assets/backend/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="assets/backend/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="assets/backend/plugins/jquery-cookie/jquery.cookie.js"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="assets/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="assets/backend/plugins/DataTables/js/jquery.dataTables.js"></script>
<script src="assets/backend/plugins/DataTables/js/dataTables.responsive.js"></script>
<script src="assets/backend/js/table-manage-responsive.demo.min.js"></script>
<script src="assets/backend/plugins/bootstrap-wysihtml5/lib/js/wysihtml5-0.3.0.js"></script>
<script src="assets/backend/plugins/bootstrap-wysihtml5/src/bootstrap-wysihtml5.js"></script>
<script src="assets/backend/js/form-wysiwyg.demo.min.js"></script>
<script src="assets/backend/js/apps.min.js"></script>
<script src="assets/backend/js/form-plugins.demo.min.js"></script>
<!-- ================== END PAGE LEVEL JS ================== -->

<script>
	$(document).ready(function() {
		App.init();
		TableManageResponsive.init();
		FormWysihtml5.init();
		FormPlugins.init();
		// FormMultipleUpload.init();
	});
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53034621-1', 'auto');
  ga('send', 'pageview');
</script>