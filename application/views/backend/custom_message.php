<!-- begin #content -->
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<!-- <ol class="breadcrumb pull-right">
		<li><a href="javascript:;">Home</a></li>
		<li><a href="javascript:;">Page Options</a></li>
		<li class="active">Page with Footer</li>
	</ol> -->
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Custom Messages sent are shown here</h1>
	<!-- end page-header -->
	
	<div class="panel panel-inverse">
	    <div class="panel-heading">
	        <div class="panel-heading-btn">
	            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
	            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
	            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
	            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
	        </div>
	        <h4 class="panel-title">Custom Message Table</h4>
	    </div>
	    <!-- <div class="alert alert-info fade in" align="right">
            <a href="#" class="btn btn-sm btn-primary">
	            <i class="fa fa-plus"> Add Event</i>
	        </a>
        </div> -->
	    <div class="panel-body table-responsive">
	        <table id="data-table" class="table table-striped table-bordered nowrap" width="100%">
	            <thead>
	                <tr>
	                    <th>#</th>
	                    <th>On</th>
	                    <th>Name</th>
	                    <th>Email</th>
	                    <th>Subject</th>
	                    <th>Message</th>
	                </tr>
	            </thead>
	            <tbody>
	            	<?php
              			$count = 1;
              			$custom_message_info = $this->db->get('custom_message')->result_array();
              			foreach ($custom_message_info as $row):
              		?>
	              	<tr>
	              		<td><?php echo $count++; ?></td>
	              		<td><?php echo date('d M, Y', $row['timestamp']); ?></td>
	              		<td><?php echo $row['name']; ?></td>
	              		<td><?php echo $row['email']; ?></td>
	              		<td><?php echo $row['subject']; ?></td>
	              		<td><?php echo $row['message']; ?></td>
	              	</tr>
	              	<?php endforeach; ?>
	            </tbody>
	        </table>
	    </div>
	</div>
</div>
<!-- end #content -->
