<!-- ================== BEGIN BASE CSS STYLE ================== -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
<link href="assets/backend/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css" rel="stylesheet" />
<link href="assets/backend/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link href="assets/backend/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="assets/backend/css/animate.min.css" rel="stylesheet" />
<link href="assets/backend/css/style.min.css" rel="stylesheet" />
<link href="assets/backend/css/style-responsive.min.css" rel="stylesheet" />
<link href="assets/backend/css/theme/default.css" rel="stylesheet" id="theme" />
<!-- ================== END BASE CSS STYLE ================== -->

<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
<link href="assets/backend/plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" />
<link href="assets/backend/plugins/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" />
<link href="assets/backend/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" />
<link href="assets/backend/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" />
<!-- ================== END PAGE LEVEL STYLE ================== -->

<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
<link href="assets/backend/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css" rel="stylesheet" />
<link href="assets/backend/plugins/jquery-file-upload/css/jquery.fileupload.css" rel="stylesheet" />
<link href="assets/backend/plugins/jquery-file-upload/css/jquery.fileupload-ui.css" rel="stylesheet" />
<link href="assets/backend/plugins/DataTables/css/data-table.css" rel="stylesheet" />
<link href="assets/backend/plugins/bootstrap-wysihtml5/src/bootstrap-wysihtml5.css" rel="stylesheet" />
<!-- ================== END PAGE LEVEL STYLE ================== -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="assets/backend/plugins/pace/pace.min.js"></script>
<!-- ================== END BASE JS ================== -->