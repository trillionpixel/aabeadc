<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li><a href="javascript:;">Home</a></li>
		<li><a href="javascript:;">Form Stuff</a></li>
		<li class="active">Form Elements</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Add Event Here</h1>
	<!-- end page-header -->
	
	<!-- begin row -->
	<div class="row">
        <div class="col-md-3"></div>
        <!-- begin col-6 -->
	    <div class="col-md-6">
	        <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-stuff-1">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Add Event Form</h4>
                </div>
                <div class="panel-body">
                    <form action="<?php echo base_url(); ?>index.php?admin/event/add" method="post" class="form-horizontal" enctype="multipart/form-data">
                    <?php
                       $event_info = $this->db->get_where('event' , array('event_id' => $event_id))->result_array(); 
                       foreach ($event_info as $row):
                    ?>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Title</label>
                            <div class="col-md-9">
                                <input value="<?php echo $row['title']; ?>" type="text" name="title" class="form-control" placeholder="Type the title of the event" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Date</label>
                            <div class="col-md-9">
                                <div class="input-group input-daterange">
                                    <input value="<?php echo date('m/d/Y' , $row['start_date']); ?>" type="text" class="form-control" name="start_date" placeholder="Date Start">
                                    <span class="input-group-addon">to</span>
                                    <input value="<?php echo date('m/d/Y' , $row['end_date']); ?>" type="text" class="form-control" name="end_date" placeholder="Date End">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Description</label>
                            <div class="col-md-9">
                                <textarea name="description" class="form-control" placeholder="Type a little description on the event" rows="5"><?php echo $row['description']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Organizer</label>
                            <div class="col-md-9">
                                <input value="<?php echo $row['organizer_name']; ?>" name="organizer_name" type="text" class="form-control" placeholder="Type name of the organizer" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Phone (Organizer)</label>
                            <div class="col-md-9">
                                <input value="<?php echo $row['organizer_phone']; ?>" name="organizer_phone" type="text" class="form-control" placeholder="Type phone number of the organizer" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Event Address</label>
                            <div class="col-md-9">
                                <input value="<?php echo $row['event_address']; ?>" name="event_address" type="text" class="form-control" placeholder="Type address of the event" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Phone (Event)</label>
                            <div class="col-md-9">
                                <input value="<?php echo $row['event_phone']; ?>" name="event_phone" type="text" class="form-control" placeholder="Type phone number for the event" />
                            </div>
                        </div>
                    <?php endforeach;?>
                        
                        <div class="form-group">
                            <label class="col-md-3 control-label"></label>
                            <div class="col-md-9">
                                <button type="submit" class="btn btn-sm btn-success">Create Event</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-6 -->
    </div>
    <!-- end row -->
</div>