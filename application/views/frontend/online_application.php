<div class="breadcrumb-box">
    <div class="breadcrumb-container">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <h3>Online Membership Application Form</h3>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <ul class="breadcrumb">
                        <li><a href="<?php echo base_url(); ?>">Home</a></li>
                        <li><a href="<?php echo base_url(); ?>index.php?main/membership_application/">Membership Application Forms</a></li>
                        <li><span class="active">Online Membership Application Form</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<section>
    <div class="container">
        <div class="row">
            <div class="box-content col-xs-12">
                <div class="theme-form-container">
                    <div class="row theme-form">
                        <div class="col-xs-12 col-sm-12">
                            <div class="theme-form-title">
                                <h3 style="text-align: center">Please fill the form to apply for Membership</h3>
                                <hr>
                            </div>
                            <form action="<?php echo base_url(); ?>index.php?main/online_application/personal_n_academic_submit/" class="form-horizontal form-ui" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <div class="col-xs-12 col-md-6">
                                        <input type="text" class="form-control required" name="first_name" placeholder="Enter First Name">
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <input type="text" class="form-control required" name="last_name" placeholder="Enter Last Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12 col-md-6">
                                        <input type="text" class="form-control" name="street_address" placeholder="Enter Street Address">
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <input type="text" class="form-control" name="city" placeholder="Enter City Name">
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <input type="text" class="form-control" name="state" placeholder="Enter State Name">
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <input type="text" class="form-control" name="zip" placeholder="Enter Zip">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12 col-md-6">
                                        <input type="text" class="form-control" name="daytime_phone" placeholder="Enter Phone Number (Daytime)">
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <input type="text" class="form-control" name="evening_phone" placeholder="Enter Phone Number (Evening)">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12 col-md-6">
                                        <input type="email" class="form-control" name="primary_email" placeholder="Enter Email Address (Primary)">
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <input type="email" class="form-control" name="secondary_email" placeholder="Enter Email Address (Secondary)">
                                    </div>
                                </div>

                                <br>

                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <div class="table-responsive">
                                            <table class="table" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            Degree (Type)
                                                        </th>
                                                        <th>
                                                            University / College
                                                        </th>
                                                        <th>
                                                            Discipline / Major
                                                        </th>
                                                        <th>
                                                            Year Completed
                                                        </th>
                                                    </tr>
                                                </thead>
                                                
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <input type="text" class="form-control required" name="degree_one">
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="university_one">
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="discipline_one">
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="year_completed_one">
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <input type="text" class="form-control" name="degree_two">
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="university_two">
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="discipline_two">
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="year_completed_two">
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <input type="text" class="form-control" name="degree_three">
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="university_three">
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="discipline_three">
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="year_completed_three">
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <br>

                                <div class="form-group theme-form-label">
                                    <div class="col-xs-12">
                                        <div class="icheckbox_flat-grey">
                                            <input class="required" id="flat-checkbox-1" type="checkbox">
                                        </div>
                                        <label class="" for="flat-checkbox-1">
                                            I certify that I understand and support the goals and objectives of AABEA and that meet the eligibility criteria to become a Member / Associate Member.
                                        </label>
                                    </div>
                                </div>

                                <br>

                                <!-- <div class="form-group">
                                    <div class="col-xs-5">
                                        <h4>Applicant Signature:</h4>
                                        <hr>
                                        You have to upload your Digital Signature here: <input type="file" name="applicant_signature" />
                                    </div>
                                    
                                    <div class="col-xs-2"></div>

                                    <div class="col-xs-5">
                                        <h4>Date: </h4>
                                        <hr>
                                        <input type="text" class="form-control" name="submit_date" placeholder="<?php echo date('d M, Y')?>" disabled="">
                                    </div>
                                </div>

                                <br> -->

                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <button type="submit" name="submit" class="btn btn-custom btn-block btn-md">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<style>
    .fileUpload {
        position: relative;
        overflow: hidden;
    }
    .fileUpload input.upload {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }

    .custom-radio-button {
        margin: 10px 0;
    }

    @media (min-width:240px) and (max-width:480px) {
        .table { border: 1px solid #FFF; }
    }
</style>


