<!-- Flex Slider -->
<link href="assets/frontend/sliders/flex/flexslider-animations.css" rel="stylesheet">

<!-- Bootstrap CSS -->
<link href="assets/frontend/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- iCheck -->
<link href="assets/frontend/css/checkbox/grey.css" rel="stylesheet">

<!-- bootstrap-select -->
<link href="assets/frontend/css/bootstrap-select.min.css" rel="stylesheet">

<!-- Icon Fonts -->
<link href="assets/frontend/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" media="screen">
<link href="assets/frontend/fonts/flaticon/flaticon.css" rel="stylesheet" media="screen">

<!-- CSS animations -->
<link href="assets/frontend/css/animate.css" rel="stylesheet">

<!-- Theme CSS -->
<link href="assets/frontend/css/styles.css" rel="stylesheet">
<link href="assets/frontend/css/header-2.css" rel="stylesheet">
<link href="assets/frontend/css/colors/mint.css" id="colors" rel="stylesheet">
<link href="assets/frontend/css/responsive.css" rel="stylesheet">
<link href="assets/frontend/js/switcher/switcher.css" rel="stylesheet">

<!--[if IE]>
<link href="assets/css/ie.css" rel="stylesheet">
<![endif]-->

<!-- Favicon -->
<link rel="icon" type="image/png" href="assets/frontend/favicon.png">
