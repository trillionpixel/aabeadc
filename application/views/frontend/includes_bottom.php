<script src="assets/frontend/js/jquery-1.12.0.min.js"></script>
<script src="assets/frontend/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/frontend/js/switcher/switcher.js"></script>
<script src="php/mcapi-subscribe/js/mailing-list.js"></script>
<script src="assets/frontend/js/classie.js"></script>
<script src="assets/frontend/js/uisearch.js"></script>
<script src="assets/frontend/js/theme-scripts/scripts-search-form.js"></script>

<script src="assets/frontend/sliders/flex/jquery.flexslider.js"></script>
<script src="assets/frontend/js/theme-scripts/scripts-flex-slider.js"></script>

<script src="assets/frontend/js/icheck.min.js"></script>
<script src="assets/frontend/js/theme-scripts/scripts-icheck.js"></script>
<script src="assets/frontend/js/bootstrap-select.min.js"></script>
<script src="assets/frontend/js/theme-scripts/scripts-bootstrap-select.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;signed_in=false&amp;sensor=false&amp;language=en"></script>
<script src="assets/frontend/js/jquery.gmap.js"></script>
<script src="assets/frontend/js/theme-scripts/scripts-google-maps.js"></script>
<script src="assets/frontend/js/theme-scripts/scripts-contact.js"></script>
<script src="assets/frontend/js/theme-scripts/scripts-tooltip.js"></script>
<script src="assets/frontend/js/theme-scripts/scripts.js"></script>