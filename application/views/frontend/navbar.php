<div id="navbar-2">
    <nav id="navbar" class="navbar navbar-default navbar-static" role="navigation">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="navbar-header">
                        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span><span class="icon-bar top-bar"></span>
                            <span class="icon-bar middle-bar"></span>
                            <span class="icon-bar bottom-bar"></span>
                        </button>
                        <a class="navbar-brand" href="<?php echo base_url(); ?>">
                            <img width="33%" style="margin-top: -7%" id="navbar-logo" src="assets/frontend/aabea-logo-resize.png" alt="">
                        </a>
                    </div>
                    
                    <div class="collapse navbar-collapse js-navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="<?php if ($page_name == 'home') echo 'active'; ?>">
                                <a href="<?php echo base_url(); ?>">Home</a>
                            </li>
                            <li class="dropdown <?php if ($page_name == 'current_board_of_directors' || $page_name == 'past_presidents_and_bios') echo 'active'; ?>">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    About Us <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="divider"></li>
                                    <li>
                                        <a href="#">
                                            Current Board of Directors
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="#">
                                            List of All past presidents and bios
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown <?php if ($page_name == 'membership_application' || $page_name == 'online_application' || $page_name == 'membership_payments' || $page_name == 'donation') echo 'active'; ?>">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    Membership <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="divider"></li>
                                    <li>
                                        <a href="<?php echo base_url(); ?>index.php?main/membership_application/">
                                            Application Forms
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="<?php echo base_url(); ?>index.php?main/membership_payments_renew/">
                                            Renew Payments
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="<?php echo base_url(); ?>index.php?main/donation/">
                                            Donation
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="dropdown <?php if ($page_name == 'gallery' || $page_name == 'events') echo 'active'; ?>">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Programs <i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu">
                                    <li class="divider"></li>
                                    <li>
                                        <a href="#">Past Event Photos and articles</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="<?php echo base_url(); ?>index.php?main/events/">Events</a>
                                    </li>
                                </ul>
                            </li>
                            
                            <li class="<?php if ($page_name == 'contact_us') echo 'active'; ?>">
                                <a href="<?php echo base_url(); ?>index.php?main/contact_us/">Contact Us</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</div>