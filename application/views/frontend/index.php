<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>AABEADC</title>
<meta name="description" content="">
<meta name="author" content="">

    <?php include 'includes_top.php'; ?>

</head>

<body>

<div id="page-wrap" class="container">
  
    <?php include 'header.php'; ?>

    <?php include 'navbar.php'; ?>

    <?php include $page_name . '.php'; ?>

    <?php include 'footer.php'; ?>

</div>

    <?php include 'includes_bottom.php'; ?>

</body>

</html>