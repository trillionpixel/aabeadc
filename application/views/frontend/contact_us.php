<div class="breadcrumb-box">
    <div class="breadcrumb-container">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <h3>Send us your message</h3>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <ul class="breadcrumb">
                        <li><a href="<?php echo base_url(); ?>">Home</a></li>
                        <li><span class="active">Contact Us</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<section>
    <div class="container">
        <div class="row clearfix">
            <div class="contact-form-3 col-xs-12">
                <div class="theme-form-container">
                    <div class="theme-form">
                        <div class="theme-form-title">
                            <h3 style="text-align: center"><span>Contact Us</span></h3>
                            <hr>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <div class="contact-info">
                                    <!-- <div class="contact-info-icon margin-button social-icons-2">
                                        <a href="#" class="icon-facebook" data-toggle="tooltip" data-placement="top" title="Facebook">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                        <a href="#" class="icon-twitter" data-toggle="tooltip" data-placement="top" title="Twitter">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                        <a href="#" class="icon-google-plus" data-toggle="tooltip" data-placement="top" title="Google Plus">
                                            <i class="fa fa-google-plus"></i>
                                        </a>
                                        <a href="#" class="icon-linkedin" data-toggle="tooltip" data-placement="top" title="LinkedIn">
                                            <i class="fa fa-linkedin"></i>
                                        </a>
                                        <a href="#" class="icon-behance" data-toggle="tooltip" data-placement="top" title="Behance">
                                            <i class="fa fa-behance"></i>
                                        </a>
                                        <a href="#" class="icon-dribbble" data-toggle="tooltip" data-placement="top" title="Dribbble">
                                            <i class="fa fa-dribbble"></i>
                                        </a>
                                        <a href="#" class="icon-whatsapp" data-toggle="tooltip" data-placement="top" title="WhatsApp">
                                            <i class="fa fa-whatsapp"></i>
                                        </a>
                                    </div> -->
                                    <div class="default-map contact-map" id="contact-address">
                                        <span id="event_address">
                                            8829 N. Westland Dr. Gaithersburg, MD 20877, <br> Email: salimhos@verizon.net, Tel # (240) 432-6152.
                                        </span>
                                    </div>
                                    <div class="box-content">
                                        <p>
                                            AABEA Treasurer, 8829 N. Westland Dr. Gaithersburg, MD 20877, <br> Email: salimhos@verizon.net, Tel # (240) 432-6152.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <form action="<?php echo base_url(); ?>index.php?main/send_message" class="form-horizontal" method="post">
                                    <div class="message-contact-box">
                                        <div class="message-contact">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-xs-12 col-sm-6">
                                            <input type="text" class="form-control" name="name" placeholder="Type your name">
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <input type="text" class="form-control" name="email" placeholder="Type your email address">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <input type="text" class="form-control" name="subject" placeholder="Type subject">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <textarea class="form-control" name="message" cols="25" rows="4" placeholder="Type message"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <button type="submit" class="btn btn-custom btn-md btn-block">Send Message</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>