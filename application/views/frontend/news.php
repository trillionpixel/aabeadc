<div class="breadcrumb-box">
    <div class="breadcrumb-container">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <h3><?php echo $this->db->get_where('news', array('news_id' => $news_id))->row()->title; ?></h3>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <ul class="breadcrumb">
                        <li><a href="<?php echo base_url(); ?>">Home</a></li>
                        <li><a href="<?php echo base_url(); ?>index.php?main/all_news/">News</a></li>
                        <li><span class="active"><?php echo $this->db->get_where('news', array('news_id' => $news_id))->row()->title; ?></span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<section>
    <div class="container">
        <div class="row clearfix">
            <div class="contact-form-1 col-xs-12">
                <div class="theme-form-container">
                <?php
                    $news_info = $this->db->get_where('news' , array('news_id' => $news_id))->result_array();
                    foreach ($news_info as $row):
                ?>
                    <div class="row theme-form">
                        <div class="col-xs-12 col-sm-12">
                            <div class="theme-form-title">
                                <h3 style="text-align: center">
                                    <span>
                                        <?php echo $row['title']; ?>
                                    </span>
                                </h3>
                                <hr>
                            </div>
                            <p>
                                <?php echo $row['body']; ?>
                            </p>
                        </div>
                    </div>
                <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- <div id="event_map_address" style="display: none">
    <?php
        // $event_address  =   $this->db->get_where('event' , array('event_id' => $event_id))->row()->event_address;
        // echo $event_address;
    ?>
</div> -->





