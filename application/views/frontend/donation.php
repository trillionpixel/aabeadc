<div class="breadcrumb-box">
    <div class="breadcrumb-container">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <h3>Send us your message</h3>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <ul class="breadcrumb">
                        <li><a href="<?php echo base_url(); ?>">Home</a></li>
                        <li><span class="active">Contact Us</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<section id="about-us">
    <div class="container" style="padding: 8% 0">
        <div class="row">
            <div class="col-xs-12">
                <h1 class="text-center">
                    <strong>If you want to donate via PayPal. Please Click below.</strong>
                </h1>
                <p class="text-center">
                    <a href="#" class="btn btn-custom-outline-inverse btn-lg">
                        <img src="assets/frontend/paypal.gif" />
                    </a>
                </p>
            </div>
        </div>
    </div>
</section>