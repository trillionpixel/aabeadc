<footer id="footer-1">
    <div class="clearfix footer-1-copyright">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <p class="text-center">
                        &copy; 
                        <?php echo date('Y'); ?>
                        AABEADC |
                        <a href="<?php echo base_url(); ?>index.php?admin/" target="_blank">
                            Admin Panel
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>