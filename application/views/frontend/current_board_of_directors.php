<div class="breadcrumb-box">
    <div class="breadcrumb-container">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <h3>List of all the Events</h3>
                </div>
                <div class="col-xs-12 col-sm-6"> 
                    <ul class="breadcrumb">
                        <li><a href="<?php echo base_url(); ?>">Home</a></li>
                        <li class="actvie">Events</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<section>
  <!-- .container -->
  <div class="container">
    <!-- .row -->
    <div class="row">
      <div class="col-xs-12">
        <h1 class="text-center title-1"><span>We help you</span> with your business</h1>
        <h3 class="text-center subtitle">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec rutrum pharetra metus a feugiat nulla laoreet ut suspendisse dignissim non massa in vestibulum.</h3>
      </div>
    </div>
    <!-- /.row -->
    <!-- .row -->
    <div class="row clearfix">
      <!-- .box-member-team -->
      <div class="box-member-team box-content clearfix col-xs-12 col-sm-4">
        <!-- .panel -->
        <div class="panel panel-default">
          <!-- .panel-heading -->
          <div class="panel-heading">
            <img src="assets/frontend/images/team/1.jpg" class="img-responsive" alt="">
          </div>
          <!-- /.panel-heading -->
          <!-- .panel-body -->
          <div class="panel-body">
            <h3>John Tyler</h3>
            <ul class="list-unstyled">
              <li><i class="fa fa-smile-o"></i> CEO</li>
              <li><i class="fa fa-envelope-o"></i> john-tyler@vixor.com</li>
            </ul>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda architecto rem, minima iste totam reiciendis, dicta cumque in alias vel laudantium at, minus repellendus tempore officia temporibus ducimus.
            </p>
            <p>
              <a class="btn btn-twitter btn-lg btn-social-icon btn-block" href="#"><i class="fa fa-twitter"></i> @john-tyler </a>
            </p>
          </div>
          <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
      </div>
      <!-- /.box-member-team -->
      <!-- .box-member-team -->
      <div class="box-member-team box-content clearfix col-xs-12 col-sm-4">
        <!-- .panel -->
        <div class="panel panel-default">
          <!-- .panel-heading -->
          <div class="panel-heading">
            <img src="assets/frontend/images/team/2.jpg" class="img-responsive" alt="">
          </div>
          <!-- /.panel-heading -->
          <!-- .panel-body -->
          <div class="panel-body">
            <h3>Ryan Jones</h3>
            <ul class="list-unstyled">
              <li><i class="fa fa-smile-o"></i> Designer</li>
              <li><i class="fa fa-envelope-o"></i> ryan-jones@vixor.com</li>
            </ul>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda architecto rem, minima iste totam reiciendis, dicta cumque in alias vel laudantium at, minus repellendus tempore officia temporibus ducimus.
            </p>
            <p>
              <a class="btn btn-twitter btn-lg btn-social-icon btn-block" href="#"><i class="fa fa-twitter"></i> @ryan-jones </a>
            </p>
          </div>
          <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
      </div>
      <!-- /.box-member-team -->
      <!-- .box-member-team -->
      <div class="box-member-team box-content clearfix col-xs-12 col-sm-4">
        <!-- .panel -->
        <div class="panel panel-default">
          <!-- .panel-heading -->
          <div class="panel-heading">
            <img src="assets/frontend/images/team/3.jpg" class="img-responsive" alt="">
          </div>
          <!-- /.panel-heading -->
          <!-- .panel-body -->
          <div class="panel-body">
            <h3>Sam Burns</h3>
            <ul class="list-unstyled">
              <li><i class="fa fa-smile-o"></i> Designer</li>
              <li><i class="fa fa-envelope-o"></i> sam-burns@vixor.com</li>
            </ul>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda architecto rem, minima iste totam reiciendis, dicta cumque in alias vel laudantium at, minus repellendus tempore officia temporibus ducimus.
            </p>
            <p>
              <a class="btn btn-twitter btn-lg btn-social-icon btn-block" href="#"><i class="fa fa-twitter"></i> @sam-burns </a>
            </p>
          </div>
          <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
      </div>
      <!-- /.box-member-team -->
      <!-- .box-member-team -->
      <div class="box-member-team box-content clearfix col-xs-12 col-sm-4">
        <!-- .panel -->
        <div class="panel panel-default">
          <!-- .panel-heading -->
          <div class="panel-heading">
            <img src="assets/frontend/images/team/4.jpg" class="img-responsive" alt="">
          </div>
          <!-- /.panel-heading -->
          <!-- .panel-body -->
          <div class="panel-body">
            <h3>Evelyn Blair</h3>
            <ul class="list-unstyled">
              <li><i class="fa fa-smile-o"></i> Designer</li>
              <li><i class="fa fa-envelope-o"></i> evelyn-blair@vixor.com</li>
            </ul>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda architecto rem, minima iste totam reiciendis, dicta cumque in alias vel laudantium at, minus repellendus tempore officia temporibus ducimus.
            </p>
            <p>
              <a class="btn btn-twitter btn-lg btn-social-icon btn-block" href="#"><i class="fa fa-twitter"></i> @evelyn-blair </a>
            </p>
          </div>
          <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
      </div>
      <!-- /.box-member-team -->
      <!-- .box-member-team -->
      <div class="box-member-team box-content clearfix col-xs-12 col-sm-4">
        <!-- .panel -->
        <div class="panel panel-default">
          <!-- .panel-heading -->
          <div class="panel-heading">
            <img src="assets/frontend/images/team/5.jpg" class="img-responsive" alt="">
          </div>
          <!-- /.panel-heading -->
          <!-- .panel-body -->
          <div class="panel-body">
            <h3>Kyle Laurent</h3>
            <ul class="list-unstyled">
              <li><i class="fa fa-smile-o"></i> Developer</li>
              <li><i class="fa fa-envelope-o"></i> kyle-laurent@vixor.com</li>
            </ul>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda architecto rem, minima iste totam reiciendis, dicta cumque in alias vel laudantium at, minus repellendus tempore officia temporibus ducimus.
            </p>
            <p>
              <a class="btn btn-twitter btn-lg btn-social-icon btn-block" href="#"><i class="fa fa-twitter"></i> @kyle-laurent </a>
            </p>
          </div>
          <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
      </div>
      <!-- /.box-member-team -->
      <!-- .box-member-team -->
      <div class="box-member-team box-content clearfix col-xs-12 col-sm-4">
        <!-- .panel -->
        <div class="panel panel-default">
          <!-- .panel-heading -->
          <div class="panel-heading">
            <img src="assets/frontend/images/team/6.jpg" class="img-responsive" alt="">
          </div>
          <!-- /.panel-heading -->
          <!-- .panel-body -->
          <div class="panel-body">
            <h3>Alison Bennet</h3>
            <ul class="list-unstyled">
              <li><i class="fa fa-smile-o"></i> Designer</li>
              <li><i class="fa fa-envelope-o"></i> alison-bennet@vixor.com</li>
            </ul>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda architecto rem, minima iste totam reiciendis, dicta cumque in alias vel laudantium at, minus repellendus tempore officia temporibus ducimus.
            </p>
            <p>
              <a class="btn btn-twitter btn-lg btn-social-icon btn-block" href="#"><i class="fa fa-twitter"></i> @alison-bennet </a>
            </p>
          </div>
          <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
      </div>
      <!-- /.box-member-team -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container -->
  </section>