<div class="breadcrumb-box">
    <div class="breadcrumb-container">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <h3><?php echo $this->db->get_where('event', array('event_id' => $event_id))->row()->title; ?></h3>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <ul class="breadcrumb">
                        <li><a href="<?php echo base_url(); ?>">Home</a></li>
                        <li><a href="<?php echo base_url(); ?>index.php?main/events/">Events</a></li>
                        <li><span class="active"><?php echo $this->db->get_where('event', array('event_id' => $event_id))->row()->title; ?></span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<section>
    <div class="container">
        <div class="row clearfix">
            <div class="contact-form-1 col-xs-12">
                <div class="theme-form-container">
                <?php
                    $event_info = $this->db->get_where('event' , array('event_id' => $event_id))->result_array();
                    foreach ($event_info as $row):
                ?>
                    <div class="row theme-form">
                        <div class="col-xs-12 col-sm-6">
                            <div class="theme-form-title">
                                <h4><span>Event Details</span></h4>
                                <hr>
                            </div>
                            <table class="table table-striped">
                                <!-- <thead>
                                    <tr>
                                        <th style="text-align: center">
                                            <?php // echo $row['title']; ?>
                                        </th>
                                    </tr>
                                </thead> -->
                                <tbody>
                                    <tr>
                                        <th scope="row">
                                            Start Date: <?php echo date('d M, Y' , $row['start_date']); ?>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th scope="row">
                                            End Date: <?php echo date('d M, Y' , $row['end_date']); ?>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th scope="row">
                                            Description: <?php echo $row['description']; ?>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th scope="row">
                                            Organizer: <?php echo $row['organizer_name']; ?>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th scope="row">
                                            Phone (Organizer): <?php echo $row['organizer_phone']; ?>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="theme-form-title">
                                <h4><span>Location Details</span></h4>
                                <hr>
                            </div>
                            <div class="default-map contact-map">
                            </div>
                            <div class="info-contact">
                                <p class="info-contact-detail">
                                    <span class="info-contact-icon"><i class="fa fa-map-marker"></i></span>
                                    <span id="event_address">
                                        <?php echo $row['event_address']; ?>
                                    </span>
                                </p>
                                <p class="info-contact-detail">
                                    <span class="info-contact-icon"><i class="fa fa-mobile-phone"></i></span> 
                                    <?php echo $row['event_phone']; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- <div id="event_map_address" style="display: none">
    <?php
        // $event_address  =   $this->db->get_where('event' , array('event_id' => $event_id))->row()->event_address;
        // echo $event_address;
    ?>
</div> -->





