<section class="flex-slider background-color-light-gray">
    <div class="container">
        <div class="row clearfix">
            <div class="col-xs-12">
                <!-- <div class="flexslider-container">
                    <div class="flexslider">
                        <div id="flexslider-slides-container">
                            <ul class="slides">
                                <li class="active" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
                                    <img src="assets/frontend/sliders/flex/images/4.jpg" alt="" data-animate-in-class="" data-animate-out-class="" draggable="false" class="animate animated">
                                </li>
                                <li class="flex-active-slide" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;">
                                    <img src="assets/frontend/sliders/flex/images/3.jpg" alt="" data-animate-in-class="" data-animate-out-class="" draggable="false" class="animate animated">
                                </li>
                                <li class="" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 3;">
                                    <img src="assets/frontend/sliders/flex/images/2.jpg" alt="" data-animate-in-class="" data-animate-out-class="" draggable="false" class="animate animated">
                                </li>
                                <li class="" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 4;">
                                    <img src="assets/frontend/sliders/flex/images/1.jpg" alt="" data-animate-in-class="" data-animate-out-class="" draggable="false" class="animate animated">
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="slider-shadow div-center">
                        <img src="assets/frontend/sliders/flex/images/slider-shadow.png" class="img-responsive" alt="">
                    </div>
                </div> -->
            </div>
        </div>
    </div>
</section>

<section class="background-color-light-gray">
    <div class="container">
        <div class="row clearfix">
            <div class="col-xs-12 col-md-3" style="margin-top: 1.8%">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th style="text-align: center">
                                Latest News
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        $news_info = $this->db->get('news')->result_array();
                        foreach($news_info as $row):
                    ?>
                        <tr>
                            <th scope="row" style="text-align: center">
                                <a href="<?php echo base_url(); ?>index.php?main/news/<?php echo $row['news_id']; ?>">
                                    <?php echo $row['title']; ?>
                                </a>
                            </th>
                        </tr>
                    <?php endforeach; ?>
                        <tr>
                            <th scope="row" style="text-align: center">
                                <a href="<?php echo base_url(); ?>index.php?main/all_news/">
                                    More...
                                </a>
                            </th>
                        </tr>
                    </tbody>
                </table>
                <div class="box-content">
                    <div class="box-wrap">
                        <img width="99%" src="assets/frontend/adhere.jpg" />
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-md-6">
                <div class="box-content">
                    <div class="box-wrap">
                        <h3 style="text-align: center">Our Mission</h3>
                        <p style="text-align: justify">
                            AABEA brings together and shares ideas, technology and experiences between engineering, 
                            architecture and computer science professionals of Bangladesh and North America. Members’
                            professional’s lives are enriched through activities such as job search and career enhancement 
                            assistance, educational seminars and recognition of achievements. AABEA fosters initiatives 
                            where the members can provide expert contribution in the areas of engineering, 
                            information technology and architecture both in the North America and Bangladesh.
                        </p>
                    </div>
                    <div class="box-wrap">
                        <h3 style="text-align: center">What is AABEA?</h3>
                        <div style="text-align: justify">
                            <p>
                                AABEA is a non-profit, non-political, and non-religious Tax-exempt voluntary scientific 
                                and technology professional organization organized exclusively for educational, 
                                scientific, and charitable programs.
                            </p>
                            <p>
                                AABEA was established in 1984 in the State of Pennsylvania. An amended constitution 
                                and by-laws ratified by the general members in May 1999 is the guiding document for 
                                the organization.
                            </p>
                            <p>
                                AABEA presently has eleven chapters: Arizona, Central Ohio, Michigan, New England, 
                                New Jersey, North Texas, Seattle, Silicon Valley, Southern California, Tristate, Washington DC
                            </p>
                        </div>
                    </div>
                    <div class="box-wrap">
                        <h3 style="text-align: center">How Are We Going To Help?</h3>
                        <div style="text-align: justify">
                            <p>With monetary donations from you, we intend to:</p>
                            <ul>
                                <li>Mobilize and engage local vendors to use local talent to build low-cost, generic, computers that are easy to deploy and maintain.</li>
                                <li>Solicit volunteer teaching staff from universities to enlighten and broaden minds through the use of computers and access to internet.</li>
                                <li>Give these underprivileged kids a fighting chance of relative prosperity in a digitally connected and ever shrinking globe.</li>
                                <li>AABEA donations are tax-deductible using Tax ID: 23-2606920</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-md-3" style="margin-top: 1.8%">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th style="text-align: center">
                                Upcoming Events
                            </th>
                        </tr>
                    </thead>
                  
                    <tbody>
                      <?php
                          $this->db->limit('5');
                          $event_info = $this->db->get('event')->result_array();
                          foreach($event_info as $row1):
                      ?>
                        <tr>
                            <th scope="row" style="text-align: center">
                                <a href="<?php echo base_url(); ?>index.php?main/event/<?php echo $row1['event_id']; ?>">
                                    <?php echo $row1['title']; ?>
                                </a>
                            </th>
                        </tr>
                      <?php endforeach; ?>
                      <tr>
                        <th scope="row" style="text-align: center">
                            <a href="<?php echo base_url(); ?>index.php?main/events/">
                                More...
                            </a>
                        </th>
                    </tr>
                    </tbody>
                </table>
                
                <div class="box-content">
                    <div class="box-wrap">
                        <img width="99%" src="assets/frontend/adhere.jpg" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>