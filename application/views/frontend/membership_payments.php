<div class="breadcrumb-box">
    <div class="breadcrumb-container">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <h3>Online Membership Application Form</h3>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <ul class="breadcrumb">
                        <li><a href="<?php echo base_url(); ?>">Home</a></li>
                        <li><a href="<?php echo base_url(); ?>index.php?main/membership_application">Membership Application Forms</a></li>
                        <li><span class="active">Online Membership Application Form</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<section>
    <div class="container">
        <div class="row">
            <div class="box-content col-xs-12">
                <div class="theme-form-container">
                    <div class="row theme-form">
                        <div class="col-xs-12 col-sm-12">
                            <div class="theme-form-title">
                                <div class="col-xs-12" align="center">
                                    <h3>Please fill the form to pay for Membership</h3>
                                    <hr>
                                </div>
                                <div class="alert alert-info col-xs-12 col-md-6" align="center" role="alert">
                                    <strong>Your Membership ID# <?php echo $membership_id; ?></strong>
                                </div>
                                <hr>
                            </div>
                            <form action="<?php echo base_url(); ?>index.php?main/membership_payments/submit/<?php echo $membership_id; ?>" class="form-horizontal form-ui" method="post">
                                <div class="form-group">
                                    <div class="col-xs-12 col-md-6">
                                        <select class="selectpicker bs-select-hidden required" name="type_of_membership">
                                            <option>Type of Membership</option>
                                            <option value="Renew">Renew</option>
                                            <option value="New">New</option>
                                        </select>
                                    </div>

                                    <div class="col-xs-12 col-md-6">
                                        <select id="time_period" class="selectpicker bs-select-hidden" name="time_period">
                                            <option>Time Period</option>
                                            <option value="One Year">One Year</option>
                                            <option value="Two Years">Two Years</option>
                                        </select>
                                    </div>
                                </div>

                                <br>

                                <div class="col-xs-12">
                                    <div class="row clearfix well">
                                        <div class="col-xs-12 col-sm-12 col-lg-12 div-center">
                                            <a target="_blank" href="<?php echo base_url(); ?>index.php?products/pay/<?php echo $membership_id; ?>/1" 
                                            class="btn btn-custom btn-lg">
                                                Pay via Paypal for One Year $25.00 USD
                                            </a>
                                            
                                            <a target="_blank" href="<?php echo base_url(); ?>index.php?products/pay/<?php echo $membership_id; ?>/50" 
                                            class="btn btn-custom btn-lg" href="#">
                                                Pay via Paypal for Two Years $50.00 USD
                                            </a>
                                            <a class="btn btn-custom btn-lg" href="#">
                                                Pay via Cheque
                                            </a>
                                        </div>

                                        <div class="col-sx-6 col-sm-6 col-lg-6 div-center">
                                            
                                        </div>
                                    </div>
                                </div>

                                <h5 style="text-align: justify">
                                    Please make check payable to AABEA-DC and mail along with 
                                    complete form to <strong>AABEA Treasurer, 8829 N. Westland Dr. Gaithersburg, MD 20877</strong>, 
                                    Email: <strong>salimhos@verizon.net</strong>, Tel # <strong>(240) 432-6152</strong>. 
                                    If you have any questions regarding membership, please email <strong>membership@aabeadc.org</strong>.
                                </h5>

                                <br>

                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <button type="submit" name="submit" class="btn btn-custom btn-block btn-md">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<style type="text/css">
    @media (min-width:240px) and (max-width:480px) {
        .btn-lg {
            font-size: 8px !important;
        }
    }
</style>


