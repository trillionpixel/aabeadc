<div class="breadcrumb-box">
    <!-- .breadcrumb-container -->
    <div class="breadcrumb-container">
      <!-- .container -->
      <div class="container">
        <!-- .row -->
        <div class="row">
          <div class="col-xs-12 col-sm-6">
            <h3>Blog Home</h3>
          </div>
          <div class="col-xs-12 col-sm-6">
            <!-- .breadcrumb -->
            <ul class="breadcrumb">
              <li><a href="index.html">Home</a></li>
              <li>Blog</li>
              <li><span class="active">Blog Home</span></li>
            </ul>
            <!-- /.breadcrumb -->
          </div>
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container -->
    </div>
    <!-- /.breadcrumb-container -->
  </div>

<section class="background-color-light-gray">
  <!-- .container -->
  <div class="container">
    <!-- .row -->
    <div class="row clearfix">
      <!-- posts -->
      <div class="col-xs-12 col-md-12">
        <!-- .row -->
        <div class="row">
          <!-- .blog-widget-box -->
          <div class="blog-widget-box clearfix col-xs-12 col-sm-4">
            <!-- .panel -->
            <div class="panel panel-default blog-widget">
              <!-- .img -->
              <div class="img">
                <div class="post-tag">
                  <a href="#"><span class="label label-default">Design</span></a>
                </div>
                <img src="assets/frontend/images/blog/1.jpg" class="img-responsive" alt="">
              </div>
              <!-- /.img -->
              <!-- .panel-body -->
              <div class="panel-body">
                <!-- .panel-title-post -->
                <div class="panel-title-post">
                  <small>February 21, 2016 / Sam Burns</small>
                  <!-- .image-caption-post-title -->
                  <div class="image-caption-post-title">
                    <p>
                      <a href="#">Lorem ipsum dolor sit amet adipisi elit doloribus blanditiis veniam</a>
                    </p>
                  </div>
                  <!-- /.image-caption-post-title -->
                </div>
                <!-- /.panel-title-post -->
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et enim veniam incidunt, nisi quo. Suscipit aliquid, officiis facilis, deserunt natus laboriosam sequi totam hic magni fugit esse eius quo dolore ipsum dolor sit amet.
                </p>
                <p class="text-right">
                  <a href="#" class="btn btn-custom-outline btn-sm">Read More</a>
                </p>
              </div>
              <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
          </div>
          <!-- /.blog-widget-box -->
          <!-- .blog-widget-box -->
          <div class="blog-widget-box clearfix col-xs-12 col-sm-4">
            <!-- .panel -->
            <div class="panel panel-default blog-widget">
              <!-- .img -->
              <div class="img">
                <div class="post-tag">
                  <a href="#"><span class="label label-default">Photography</span></a>
                </div>
                <img src="assets/frontend/images/blog/2.jpg" class="img-responsive" alt="">
              </div>
              <!-- /.img -->
              <!-- .panel-body -->
              <div class="panel-body">
                <!-- .panel-title-post -->
                <div class="panel-title-post">
                  <small>February 21, 2016 / Sam Burns</small>
                  <!-- .image-caption-post-title -->
                  <div class="image-caption-post-title">
                    <p>
                      <a href="#">Lorem ipsum dolor sit amet adipisi elit doloribus blanditiis veniam</a>
                    </p>
                  </div>
                  <!-- /.image-caption-post-title -->
                </div>
                <!-- /.panel-title-post -->
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et enim veniam incidunt, nisi quo. Suscipit aliquid, officiis facilis, deserunt natus laboriosam sequi totam hic magni fugit esse eius quo dolore ipsum dolor sit amet.
                </p>
                <p class="text-right">
                  <a href="#" class="btn btn-custom-outline btn-sm">Read More</a>
                </p>
              </div>
              <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
          </div>
          <!-- /.blog-widget-box -->
          <!-- .blog-widget-box -->
          <div class="blog-widget-box clearfix col-xs-12 col-sm-4">
            <!-- .panel -->
            <div class="panel panel-default blog-widget">
              <!-- .img -->
              <div class="img">
                <div class="post-tag">
                  <a href="#"><span class="label label-default">Photoshop</span></a>
                </div>
                <img src="assets/frontend/images/blog/3.jpg" class="img-responsive" alt="">
              </div>
              <!-- /.img -->
              <!-- .panel-body -->
              <div class="panel-body">
                <!-- .panel-title-post -->
                <div class="panel-title-post">
                  <small>February 21, 2016 / Sam Burns</small>
                  <!-- .image-caption-post-title -->
                  <div class="image-caption-post-title">
                    <p>
                      <a href="#">Lorem ipsum dolor sit amet adipisi elit doloribus blanditiis veniam</a>
                    </p>
                  </div>
                  <!-- /.image-caption-post-title -->
                </div>
                <!-- /.panel-title-post -->
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et enim veniam incidunt, nisi quo. Suscipit aliquid, officiis facilis, deserunt natus laboriosam sequi totam hic magni fugit esse eius quo dolore ipsum dolor sit amet.
                </p>
                <p class="text-right">
                  <a href="#" class="btn btn-custom-outline btn-sm">Read More</a>
                </p>
              </div>
              <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
          </div>
          <!-- /.blog-widget-box -->
          <!-- .blog-widget-box -->
          <div class="blog-widget-box clearfix clearfix col-xs-12 col-sm-4">
            <!-- .panel -->
            <div class="panel panel-default blog-widget">
              <!-- .img -->
              <div class="img">
                <div class="post-tag">
                  <a href="#"><span class="label label-default">Bootstrap</span></a>
                </div>
                <img src="assets/frontend/images/blog/4.jpg" class="img-responsive" alt="">
              </div>
              <!-- /.img -->
              <!-- .panel-body -->
              <div class="panel-body">
                <!-- .panel-title-post -->
                <div class="panel-title-post">
                  <small>February 21, 2016 / Sam Burns</small>
                  <!-- .image-caption-post-title -->
                  <div class="image-caption-post-title">
                    <p>
                      <a href="#">Lorem ipsum dolor sit amet adipisi elit doloribus blanditiis veniam</a>
                    </p>
                  </div>
                  <!-- /.image-caption-post-title -->
                </div>
                <!-- /.panel-title-post -->
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et enim veniam incidunt, nisi quo. Suscipit aliquid, officiis facilis, deserunt natus laboriosam sequi totam hic magni fugit esse eius quo dolore ipsum dolor sit amet.
                </p>
                <p class="text-right">
                  <a href="#" class="btn btn-custom-outline btn-sm">Read More</a>
                </p>
              </div>
              <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
          </div>
          <!-- /.blog-widget-box -->
          <!-- .blog-widget-box -->
          <div class="blog-widget-box clearfix col-xs-12 col-sm-4">
            <!-- .panel -->
            <div class="panel panel-default blog-widget">
              <!-- .img -->
              <div class="img">
                <div class="post-tag">
                  <a href="#"><span class="label label-default">Downloads</span></a>
                </div>
                <img src="assets/frontend/images/blog/5.jpg" class="img-responsive" alt="">
              </div>
              <!-- /.img -->
              <!-- .panel-body -->
              <div class="panel-body">
                <!-- .panel-title-post -->
                <div class="panel-title-post">
                  <small>February 21, 2016 / Sam Burns</small>
                  <!-- .image-caption-post-title -->
                  <div class="image-caption-post-title">
                    <p>
                      <a href="#">Lorem ipsum dolor sit amet adipisi elit doloribus blanditiis veniam</a>
                    </p>
                  </div>
                  <!-- /.image-caption-post-title -->
                </div>
                <!-- /.panel-title-post -->
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et enim veniam incidunt, nisi quo. Suscipit aliquid, officiis facilis, deserunt natus laboriosam sequi totam hic magni fugit esse eius quo dolore ipsum dolor sit amet.
                </p>
                <p class="text-right">
                  <a href="#" class="btn btn-custom-outline btn-sm">Read More</a>
                </p>
              </div>
              <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
          </div>
          <!-- /.blog-widget-box -->
          <!-- .blog-widget-box -->
          <div class="blog-widget-box clearfix col-xs-12 col-sm-4">
            <!-- .panel -->
            <div class="panel panel-default blog-widget">
              <!-- .img -->
              <div class="img">
                <div class="post-tag">
                  <a href="#"><span class="label label-default">Tips</span></a>
                </div>
                <img src="assets/frontend/images/blog/6.jpg" class="img-responsive" alt="">
              </div>
              <!-- /.img -->
              <!-- .panel-body -->
              <div class="panel-body">
                <!-- .panel-title-post -->
                <div class="panel-title-post">
                  <small>February 21, 2016 / Sam Burns</small>
                  <!-- .image-caption-post-title -->
                  <div class="image-caption-post-title">
                    <p>
                      <a href="#">Lorem ipsum dolor sit amet adipisi elit doloribus blanditiis veniam</a>
                    </p>
                  </div>
                  <!-- /.image-caption-post-title -->
                </div>
                <!-- /.panel-title-post -->
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et enim veniam incidunt, nisi quo. Suscipit aliquid, officiis facilis, deserunt natus laboriosam sequi totam hic magni fugit esse eius quo dolore ipsum dolor sit amet.
                </p>
                <p class="text-right">
                  <a href="#" class="btn btn-custom-outline btn-sm">Read More</a>
                </p>
              </div>
              <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
          </div>
          <!-- /.blog-widget-box -->
        </div>
        <!-- /.row -->
        
      </div>
      <!-- /posts -->
      
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container -->
  </section>