
<div class="breadcrumb-box">
    <div class="breadcrumb-container">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <h3>Membership Application Forms</h3>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <ul class="breadcrumb">
                        <li><a href="<?php echo base_url(); ?>">Home</a></li>
                        <li><span class="active">Membership Application Forms</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="background-color-light-gray">
    <div class="container">
        <div class="row clearfix" style="margin: 3% 0 11% 0">
            <div class="col-xs-12">
                <div class="box-content">
                    <div class="box-wrap">
                    <p style="text-align: justify;">
                        American Association of Bangladeshi Engineers and Architects (AABEA) is a registered, professional, 
                        non‐profit and non‐political organization for engineers, architects, information technology professionals 
                        of Bangladeshi descent in North America. In accordance with Article 4, Section 1 of the AABEA Constitution, a Member shall have, 
                        at a minimum, a bachelor’s degree in an engineering discipline or approved equivalent. A person with a diploma in engineering, 
                        certification or associate’s degree, 
                        or a student of engineering discipline may apply to become an Associate Member. We look forward to your joining us!
                    </p>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row clearfix">
            <div class="col-xs-6">
                <div class="box-content">
                    <div class="box-wrap" align="center">
                        <h4>Click here for Online Version</h4>
                        <a href="<?php echo base_url(); ?>index.php?main/online_application/" class="btn btn-md btn-primary">Application Form</a>
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="box-content">
                    <div class="box-wrap" align="center">
                        <h4>Click here for PDF Version</h4>
                        <a href="assets/frontend/Membership_Application_Form.pdf" target="_blank" class="btn btn-md btn-primary">Application Form</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<style type="text/css">
    @media (min-width:240px) and (max-width:480px) {
        .btn-md {
            font-size: 10px !important;
        }
    }
</style>