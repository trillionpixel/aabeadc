<section id="top-bar">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="top-bar-info-content col-xs-12 col-sm-6">
                        <div class="social-icons">
                            <a target="_blank" href="https://www.facebook.com/Aabea-DC-1754889944732054/" class="icon-facebook" title="">
                                <i class="fa fa-facebook"></i>
                            </a>
                            <!-- <a href="#" class="icon-twitter" title="">
                                <i class="fa fa-twitter"></i>
                            </a>
                            <a href="#" class="icon-google-plus" title="">
                                <i class="fa fa-google-plus"></i>
                            </a> -->
                        </div>
                    </div>
                    
                    <div style="display: none" class="top-bar-search-content col-xs-12 col-sm-3 col-md-4">
                        <div id="sb-search" class="sb-search">
                            <form>
                                <input class="sb-search-input" placeholder="Search..." type="text" value="" name="search" id="search">
                                <input class="sb-search-submit" type="submit" value="">
                                <span class="sb-icon-search"></span>
                            </form>
                        </div>
                    </div>
          
                    <div style="display: none" class="top-bar-links-content col-xs-12 col-sm-3 col-md-2">
                        <ul class="list-unstyled list-inline">
                            <li><a href="#"><i class="fa fa-lock"></i> Sign in</a></li>
                            <li><a href="#">Sign up</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>