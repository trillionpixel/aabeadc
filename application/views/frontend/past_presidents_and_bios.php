<div class="breadcrumb-box">
    <div class="breadcrumb-container">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <h3>List of all the Events</h3>
                </div>
                <div class="col-xs-12 col-sm-6"> 
                    <ul class="breadcrumb">
                        <li><a href="<?php echo base_url(); ?>">Home</a></li>
                        <li class="actvie">Events</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<section>
  <!-- .container -->
  <div class="container">
    <!-- .row -->
    <div class="row">
      <div class="col-xs-12">
        <h1 class="text-center title-1"><span>They Say</span> About Us</h1>
        <h3 class="text-center subtitle">Vivamus dictum sapien scelerisque convallis enim bibendum semper ipsum. Vestibulum eget lacus arcu curabitur pharetra laoreet gravida sed malesuada porta.</h3>
        <!-- .row -->
        <div class="row clearfix">
          <!-- .testimonial-box -->
          <div class="testimonial-box col-xs-12 col-sm-6">
            <!-- .well -->
            <div class="well">
              <p class="testimonial">
                <img src="assets/frontend/images/testimonials/1.png" class="pull-left img-pl-margin img-thumbnail" alt=""> "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iure quae consequuntur facere eos, sint enim optio. Reiciendis ab consequatur, facilis similique quam quis dolore praesentium".
              </p>
              <p class="customer-name social-icons-2 margin-button">
                Andrew Johnson.
                <a href="#" class="icon-linkedin" data-toggle="tooltip" data-placement="top" title="" data-original-title="LinkedIn">
                    <i class="fa fa-linkedin"></i>
                </a>
              </p>
            </div>
            <!-- /.well -->
          </div>
          <!-- /.testimonial-box -->
          <!-- .testimonial-box -->
          <div class="testimonial-box col-xs-12 col-sm-6">
            <!-- .well -->
            <div class="well">
              <p class="testimonial text-right">
                <img src="assets/frontend/images/testimonials/2.png" class="pull-right img-pr-margin img-thumbnail" alt=""> "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iure quae consequuntur facere eos, sint enim optio. Reiciendis ab consequatur, facilis similique quam quis dolore praesentium".
              </p>
              <p class="customer-name text-right social-icons-2 margin-button">
                Scott Turner.
                <a href="#" class="icon-linkedin" data-toggle="tooltip" data-placement="top" title="" data-original-title="LinkedIn">
                    <i class="fa fa-linkedin"></i>
                </a>
              </p>
            </div>
            <!-- /.well -->
          </div>
          <!-- /.testimonial-box -->
        </div>
        <!-- /.row -->
        <!-- .row -->
        <div class="row clearfix">
          <!-- .testimonial-box -->
          <div class="testimonial-box col-xs-12 col-sm-6">
            <!-- .well -->
            <div class="well">
              <p class="testimonial">
                <img src="assets/frontend/images/testimonials/3.png" class="pull-left img-pl-margin img-thumbnail" alt=""> "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iure quae consequuntur facere eos, sint enim optio. Reiciendis ab consequatur, facilis similique quam quis dolore praesentium".
              </p>
              <p class="customer-name social-icons-2 margin-button">
                Rose Wood.
                <a href="#" class="icon-linkedin" data-toggle="tooltip" data-placement="top" title="" data-original-title="LinkedIn">
                    <i class="fa fa-linkedin"></i>
                </a>
              </p>
            </div>
            <!-- /.well -->
          </div>
          <!-- /.testimonial-box -->
          <!-- .testimonial-box -->
          <div class="testimonial-box col-xs-12 col-sm-6">
            <!-- .well -->
            <div class="well">
              <p class="testimonial text-right">
                <img src="assets/frontend/images/testimonials/4.png" class="pull-right img-pr-margin img-thumbnail" alt=""> "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iure quae consequuntur facere eos, sint enim optio. Reiciendis ab consequatur, facilis similique quam quis dolore praesentium".
              </p>
              <p class="customer-name text-right social-icons-2 margin-button">
                Peter Jhonson.
                <a href="#" class="icon-linkedin" data-toggle="tooltip" data-placement="top" title="" data-original-title="LinkedIn">
                    <i class="fa fa-linkedin"></i>
                </a>
              </p>
            </div>
            <!-- /.well -->
          </div>
          <!-- /.testimonial-box -->
        </div>
        <!-- /.row -->
      </div>
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container -->
  </section>