<div class="breadcrumb-box">
    <div class="breadcrumb-container">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <h3>List of all the Events</h3>
                </div>
                <div class="col-xs-12 col-sm-6"> 
                    <ul class="breadcrumb">
                        <li><a href="<?php echo base_url(); ?>">Home</a></li>
                        <li class="actvie">Events</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="background-color-light-gray">
    <div class="container">
        <div class="row clearfix">
            <div class="col-xs-12 col-md-8">
                <div class="row">
                <?php
                    $news_info = $this->db->get('news')->result_array();
                    foreach ($news_info as $row):
                ?>
                    <div class="blog-widget-box clearfix col-xs-12 col-sm-12">
                        <div class="panel panel-default blog-widget">
                            <div class="panel-body">
                                <div class="panel-title-post">
                                    <div class="image-caption-post-title">
                                        <p>
                                            <a href="<?php echo base_url(); ?>index.php?main/news/<?php echo $row['news_id']; ?>">
                                                <?php echo $row['title']; ?>
                                            </a>

                                            <p>
                                                <small>Published on <?php echo date('d M, Y' , $row['timestamp']); ?></small>
                                            </p>

                                            <p class="text-right">
                                                <a href="<?php echo base_url(); ?>index.php?main/event/<?php echo $row['news_id']; ?>" class="btn btn-custom-outline btn-sm">
                                                    Read More
                                                </a>
                                            </p>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th style="text-align: center">
                                Latest News
                            </th>
                        </tr>
                    </thead>
            
                    <tbody>
                    <?php
                        foreach ($news_info as $row2):
                    ?>
                         <tr>
                            <td scope="row" style="text-align: center">
                                <a href="<?php echo base_url(); ?>index.php?main/news/<?php echo $row2['news_id']; ?>">
                                    <?php echo $row2['title']; ?>    
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>