<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function index()
	{
		if ($this->session->userdata('user_type') != 'admin')
		{
			redirect( base_url() . 'index.php?admin/login' , 'refresh');
		}
		
		$page_data['page_name']			=	'event';

		$this->load->view('backend/index' , $page_data);
	}

	function event($param1 = '' , $param2 = '')
	{
		if ($this->session->userdata('user_type') != 'admin')
		{
			redirect( base_url() . 'index.php?admin/login' , 'refresh');
		}
		
		if ($param1 == 'add')
		{
			$data['title']				=	$this->input->post('title');
			$data['start_date']			=	strtotime($this->input->post('start_date'));
			$data['end_date']			=	strtotime($this->input->post('end_date'));
			$data['description']		=	$this->input->post('description');
			$data['organizer_name']		=	$this->input->post('organizer_name');
			$data['organizer_phone']	=	$this->input->post('organizer_phone');
			$data['event_address']		=	$this->input->post('event_address');
			$data['event_phone']		=	$this->input->post('event_phone');
			$data['timestamp']			=	time();

			$this->db->insert('event' , $data);

			redirect(base_url() . 'index.php?admin/event/' , 'refresh');
		}

		$page_data['page_name']			=	'event';

		$this->load->view('backend/index' , $page_data);
	}

	function add_event()
	{
		if ($this->session->userdata('user_type') != 'admin')
		{
			redirect( base_url() . 'index.php?admin/login' , 'refresh');
		}
		
		$page_data['page_name']			=	'add_event';

		$this->load->view('backend/index' , $page_data);
	}

	function edit_event($param1 = '' , $param2 = '')
	{
		if ($this->session->userdata('user_type') != 'admin')
		{
			redirect( base_url() . 'index.php?admin/login' , 'refresh');
		}
		
		if ($param1 == 'edit')
		{
			$data['title']				=	$this->input->post('title');
			$data['start_date']			=	strtotime($this->input->post('start_date'));
			$data['end_date']			=	strtotime($this->input->post('end_date'));
			$data['description']		=	$this->input->post('description');
			$data['organizer_name']		=	$this->input->post('organizer_name');
			$data['organizer_phone']	=	$this->input->post('organizer_phone');
			$data['event_address']		=	$this->input->post('event_address');
			$data['event_phone']		=	$this->input->post('event_phone');
			$data['timestamp']			=	time();

			$this->db->where('event_id' , $param2);
			$this->db->update('event' , $data);

			redirect(base_url() . 'index.php?admin/event/' , 'refresh');
		}
		
		$page_data['page_name']			=	'edit_event';
		$page_data['event_id']			=	$param1;

		$this->load->view('backend/index' , $page_data);
	}

	function news($param1 = '' , $param2 = '')
	{
		if ($this->session->userdata('user_type') != 'admin')
		{
			redirect( base_url() . 'index.php?admin/login' , 'refresh');
		}
		
		if ($param1 == 'add')
		{
			$data['title']				=	$this->input->post('title');
			$data['body']				=	$this->input->post('body');
			$data['timestamp']			=	time();

			$this->db->insert('news' , $data);

			redirect(base_url() . 'index.php?admin/news/' , 'refresh');
		}

		$page_data['page_name']			=	'news';

		$this->load->view('backend/index' , $page_data);
	}

	function add_news()
	{
		if ($this->session->userdata('user_type') != 'admin')
		{
			redirect( base_url() . 'index.php?admin/login' , 'refresh');
		}
		
		$page_data['page_name']			=	'add_news';

		$this->load->view('backend/index' , $page_data);
	}

	function edit_news($param1 = '' , $param2 = '')
	{
		if ($this->session->userdata('user_type') != 'admin')
		{
			redirect( base_url() . 'index.php?admin/login' , 'refresh');
		}
		
		if ($param1 == 'edit')
		{
			$data['title']				=	$this->input->post('title');
			$data['body']				=	$this->input->post('body');
			$data['timestamp']			=	time();

			$this->db->where('news_id' , $param2);
			$this->db->update('news' , $data);

			redirect(base_url() . 'index.php?admin/news/' , 'refresh');
		}

		$page_data['page_name']			=	'edit_news';
		$page_data['news_id']			=	$param1;

		$this->load->view('backend/index' , $page_data);
	}

	function custom_message()
	{
		if ($this->session->userdata('user_type') != 'admin')
		{
			redirect( base_url() . 'index.php?admin/login' , 'refresh');
		}
		
		$page_data['page_name']			=	'custom_message';

		$this->load->view('backend/index' , $page_data);
	}

	function login()
	{
		$this->load->view('backend/login');
	}
}
