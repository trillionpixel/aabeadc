<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		$page_data['page_name']	=	'home';
		
		$this->load->view('frontend/index.php', $page_data);
	}

	// Backend SignIn
	function admin_signin()
	{
		$email				=	$this->input->post('email');
		$password			=	$this->input->post('password');
		$query				=	$this->db->get_where('admin' , array('email' => $email , 'password' => $password));

		if ($query->num_rows() > 0) // matches with db admin table 
		{
			$admin_row			=	$query->row();

			$this->session->set_userdata('admin_id' , $admin_row->admin_id);
			$this->session->set_userdata('user_type' , 'admin');

			redirect(base_url() . 'index.php?admin/' , 'refresh');
		}
		else 
			redirect(base_url() . 'index.php?admin/login' , 'refresh');
	}

	// Backend SignOut
	function admin_signout()
	{
		$this->session->unset_userdata('admin_id');
		$this->session->unset_userdata('user_type');
		
		redirect(base_url() . 'index.php?admin/login' , 'refresh');
	}

}