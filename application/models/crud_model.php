<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crud_model extends CI_Model {

	function __construct()
    {
        parent::__construct();
    }
	
	function personal_n_academic_submit()
	{
		$data['membership_id']			=	rand(1,10000);
		$data['first_name']				=	$this->input->post('first_name');
		$data['last_name']				=	$this->input->post('last_name');
		$data['street_address']			=	$this->input->post('street_address');
		$data['city']					=	$this->input->post('city');
		$data['state']					= 	$this->input->post('state');
		$data['zip']					=	$this->input->post('zip');
		$data['daytime_phone']			=	$this->input->post('daytime_phone');
		$data['evening_phone']			=	$this->input->post('evening_phone');
		$data['primary_email']			=	$this->input->post('primary_email');
		$data['secondary_email']		=	$this->input->post('secondary_email');
		// $data['applicant_signature']	=	$this->input->post('applicant_signature');
		$data['submit_date']			=	time();
		
		$data2['membership_id']			=	$data['membership_id'];
		$data2['degree_one']			=	$this->input->post('degree_one');
		$data2['university_one']		=	$this->input->post('university_one');
		$data2['discipline_one']		=	$this->input->post('discipline_one');
		$data2['year_completed_one']	=	$this->input->post('year_completed_one');
		$data2['degree_two']			=	$this->input->post('degree_two');
		$data2['university_two']		=	$this->input->post('university_two');
		$data2['discipline_two']		=	$this->input->post('discipline_two');
		$data2['year_completed_two']	=	$this->input->post('year_completed_two');
		$data2['degree_one']			=	$this->input->post('degree_three');
		$data2['university_three']		=	$this->input->post('university_three');
		$data2['discipline_three']		=	$this->input->post('discipline_three');
		$data2['year_completed_three']	=	$this->input->post('year_completed_three');

		$this->db->insert('personal_info' , $data);
		$this->db->insert('academic_info' , $data2);

		// move_uploaded_file($data['applicant_signature'], 'uploads/signatures/'. $data['applicant_signature']);

		redirect(base_url() . 'index.php?main/membership_payments/' . $data['membership_id'] , 'refresh');
	}

}